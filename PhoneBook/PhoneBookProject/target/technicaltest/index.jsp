<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
    <title>PhoneBook</title>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <script src="js/phonebook.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
    <script type="text/javascript">
        var app = angular.module("ContactManagement", []);
 
        app.controller("ContactManagementController", function($scope, $http) {

            $scope.contacts = [];
            $scope.form = {
                id : -1,
                firstName : "",
                lastName : "",
                phone : ""
            };

             _refreshPageData();

            $scope.searchContact = function (inputValue) {
                $http({
                    method : 'GET',
                    url : 'http://localhost:8080/technicaltest/rest/contacts?firstName='+ inputValue
                }).then(function successCallback(response) {
                    $scope.contacts = response.data;
                    $scope.$apply()
                }, function errorCallback(response) {
                    console.log(response.status);
                });
            }

            $scope.submitEmployee = function() {

                $http({
                    method : "POST",
                    url : "http://localhost:8080/technicaltest/rest/contact/add",
                    data : angular.toJson($scope.form),
                    headers : {
                        'Content-Type' : 'application/json'
                    }
                }).then( _success, _error );
            };

            function _refreshPageData() {
                $http({
                    method : 'GET',
                    url : 'http://localhost:8080/technicaltest/rest/contacts'
                }).then(function successCallback(response) {
                    $scope.contacts = response.data;
                    $scope.$apply()
                }, function errorCallback(response) {
                    console.log(response.status);
                });
            }


            function _success(response) {
                _refreshPageData();
                _clearForm()
            }

            function _error(response) {
                console.log(response.statusText);
            }

            //Clear the form
            function _clearForm() {
                $scope.form.firstName = "";
                $scope.form.lastName = "";
                $scope.form.phone = "";
            };
        });
    </script>
</head>
<body ng-app="ContactManagement" ng-controller="ContactManagementController"><div class="container">
    <div class="pure-g">
        <div class="pure-u-1">
            <div class="header">
                <img class="logo" src="img/phonebook.png"/>
                <p>v 1.0</p>
            </div>

        </div>
    </div>
    <div class="pure-g">
        <div class="pure-u-sm-1 pure-u-1-3">
            <div class="box">
                <h2><i class="fa fa-user-plus"></i>New contact</h2>
                <form ng-submit="submitEmployee()"  class="pure-form">
                    <fieldset class="pure-group">
                        <input type="text" class="pure-input-1-2" placeholder="First Name" ng-model="form.firstName">
                        <input type="text" class="pure-input-1-2" placeholder="Last Name" ng-model="form.lastName">
                        <input type="text" class="pure-input-1-2" placeholder="Phone" ng-model="form.phone">
                    </fieldset>
                    <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
                        <i class="fa fa-user-plus"></i>Add</button>
                </form>
            </div>
        </div>
        <div class="pure-u-sm-1 pure-u-1-3">
            <div class="box">
                <h2><i class="fa fa-search"></i>Search contact</h2>
                <form ng-submit="searchContact(inputValue)" class="pure-form">
                    <fieldset class="pure-group">
                        <input type="text" class="pure-input-1-2" ng-model="inputValue" >
                    </fieldset>
                    <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
                        <i class="fa fa-search"></i>Search</button>
                </form>
            </div>
        </div>
        <div class="pure-u-sm-1 pure-u-1-3">
            <div class="box">
                <h2><i class="fa fa-users"></i> Contacts</h2>
                <table class="pure-table">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr ng-repeat="employee in contacts.data">
                        <td>{{ employee.firstName }}</td>
                        <td>{{ employee.lastName }}</td>
                        <td>{{ employee.phone }}</td>
                     </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>