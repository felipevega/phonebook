CREATE SCHEMA IF NOT EXISTS public;

CREATE TABLE contact(
  id int,
  firstName varchar,
  lastName varchar,
  phone varchar
);

INSERT INTO contact (id, firstName, lastName, phone) VALUES (1, 'Arun', 'Kart', '415-8679089' );
INSERT INTO contact (id, firstName, lastName, phone) VALUES (2, 'Juan', 'Torus', '301-2390930' );
INSERT INTO contact (id, firstName, lastName, phone) VALUES (3, 'Nolux', 'Fernandez', '310-2930291' );
