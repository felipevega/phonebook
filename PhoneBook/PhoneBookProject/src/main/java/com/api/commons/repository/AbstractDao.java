package com.api.commons.repository;
import java.util.HashMap;
import java.util.Map;

import com.api.commons.exception.QueryNotFoundException;
import lombok.AccessLevel;
import lombok.Getter;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
/**
 * Clase Abstracta para la construcción de DAOs
 *
 * @param <E> Tipo de dato esperado en el retorno de una query
 */
public abstract class AbstractDao<E> {
    @Getter(AccessLevel.PRIVATE)
    private Class<E> entityClass;

    private Map<String, String> queries;

    @Getter(AccessLevel.PRIVATE)
    private SessionFactory sessionFactory;

    /**
     * Constructor que setea el objeto a retornar, sessionFactory y mapa de queries
     *
     * @param entityClass    The type of the DAO that would be created
     * @param sessionFactory the SessionFactory used for this DAO
     * @param q              The list of queries that would be available to check
     */
    protected AbstractDao(Class<E> entityClass, SessionFactory sessionFactory, Map<String, String> q) {
        this.entityClass = entityClass;
        this.sessionFactory = sessionFactory;
        setQueries(q);
    }

    /**
     * Constructor por defecto
     */
    protected AbstractDao() {
    }

    /**
     * Obtiene la session actual
     *
     * @return retorna una instancia de Session
     */
    protected Session getCurrentSession() {
        return getSessionFactory().getCurrentSession();
      /*  Session sesion;
        try {
            sesion = getSessionFactory().getCurrentSession();
        } catch (HibernateException e) {
            sesion = getSessionFactory().openSession();
        }
        return sesion;*/
    }

    /**
     * Obtiene quieries sql
     *
     * @return Un mapa donde la una llave esta asociada a una query sql
     */
    protected Map<String, String> getQueries() {
        if (this.queries == null) {
            this.queries = new HashMap<String, String>();
        }

        return this.queries;
    }

    /**
     * Find the methodName query in the list of queries defined in the
     * constructor
     *
     * @param methodName name of the query to find
     * @return SQLQuery with an instance of the sql query defined in the bean
     * file
     * @throws E Cuando no encuentra la query en el mapeo xml
     */
    protected SQLQuery createSqlQuery(String methodName)
            throws QueryNotFoundException {
        String sql = getQueries().get(methodName);

        if (sql == null) {
            throw new QueryNotFoundException("La query [" + methodName
                    + "] no ha sido encontrada");
        }

        return getCurrentSession().createSQLQuery(sql);
    }

    /**
     * Crea un SqlQuery basandose en la logica del método {@link AbstractDao#createSqlQuery(String)}
     * asociando a la respuesta la entidad generica E de la clase.
     *
     * @param methodName nombre de la query
     * @return retorna un SqlQuery basado en createSqlQuery con un {@link SQLQuery#addEntity(Class)}
     * @throws QueryNotFoundException Si no se encuentra la query en el mapa de queries
     */
    protected SQLQuery createSqlQueryWithEntity(String methodName) throws QueryNotFoundException {
        return createSqlQuery(methodName).addEntity(getEntityClass());
    }

    private void setQueries(Map<String, String> q) {
        this.queries = q != null ? q : new HashMap<String, String>();
    }

}
