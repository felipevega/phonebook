package com.api.commons.interceptors;

 import com.api.commons.interceptors.entity.TrackingInformation;
 import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

import static com.api.commons.interceptors.entity.TrackingInformation.REQ_ID;

@Provider
@PreMatching
public class TrackingInformationInterceptor implements ContainerRequestFilter {

    TrackingInformation trackinginformation;

    @Autowired
    public TrackingInformationInterceptor(TrackingInformation trackingInformation) {
        this.trackinginformation = trackingInformation;
    }

    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        trackinginformation.setReqId(containerRequestContext.getHeaderString(REQ_ID));
    }
}
