package com.api.commons.repository;



import com.api.commons.repository.exceptions.NotFoundException;

import java.io.Serializable;
import java.util.List;

public interface RoDao<T> {

    /**
     * Finds all entities
     * @return
     */
    List<T> all();
    /**
     * Find entities having a property (name) with given value
     *
     * @param name
     * @param value
     * @return
     */
    List<T> findBy(String name, Object value);

    /**
     * Find an unique entity having a property (name) with given value
     *
     * @param name
     * @param value
     * @return
     */
    T findUniqueBy(String name, Object value) throws NotFoundException;

    /**
     * Find an entity by its identifier
     *
     * @param id entity identifier
     * @return
     */
    T findById(Serializable id) throws NotFoundException;
}
