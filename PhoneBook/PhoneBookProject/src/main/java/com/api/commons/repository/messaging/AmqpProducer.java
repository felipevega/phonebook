package com.api.commons.repository.messaging;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Slf4j
@Data
@Component
public class AmqpProducer {

    private ConnectionFactory connectionFactory;

    @Value("${com.queue.centralRabbit.port}")
    private int port;
    @Value("${com.queue.centralRabbit.host}")
    private String host;
    @Value("${com.queue.centralRabbit.username}")
    private String username;
    @Value("${com.queue.centralRabbit.password}")
    private String password;
    @Value("${com.queue.centralRabbit.virtualHost}")
    private String virtualHost;
    @Value("${com.queue.centralRabbit.exchange}")
    private String exchange;
    @Value("${com.queue.centralRabbit.routingKey}")
    private String routingKey;

    public AmqpProducer() {

    }

    @PostConstruct
    public void init(){
        ConnectionFactory factory = new ConnectionFactory();

        factory.setHost(this.host);
        factory.setPort(this.port);
        factory.setUsername(this.username);
        factory.setPassword(this.password);
        factory.setVirtualHost(this.virtualHost);

        setConnectionFactory(factory);
        log.info("Connecting  to: [{}] port [{}] ", factory.getHost(),
                factory.getPort());

    }

    public <T extends Queueable> void sendMessage(T message) throws IOException {

        Connection connection = getConnectionFactory().newConnection();
        log.debug("Connection created to ");
        Channel channel = connection.createChannel();
        log.debug("Channel created");

        try {
            log.info(
                    "Sending a message to exchange [{}] with routing key [{}]",
                    exchange, routingKey);
            channel.basicPublish(exchange, routingKey, null,
                    message.getMessage());
        } catch (IOException e) {
            log.error(
                    "Se produjo un error al enviar el mensaje, por lo tanto se reintenta",
                    e);
            try {
                channel.basicPublish(exchange, routingKey, null, message.getMessage());
            } catch (IOException e1) {
                log.error("No se puede enviar el mensaje despues del reintento", e1);
            }
        }
        channel.close();
        connection.close();
    }
}