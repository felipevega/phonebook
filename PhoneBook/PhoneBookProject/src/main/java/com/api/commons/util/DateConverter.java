package com.api.commons.util;

 import com.api.commons.exception.PhoneBookException;
 import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Locale;

@Slf4j
public class DateConverter {

    public static DateTime isoToDateTime (String isoDateTime) throws PhoneBookException {
        DateTime dateTime;
        try{
            log.info("action=\"isoToDateTime\", description=\"method called\", isoDateTime=\"{}\"", isoDateTime);

            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZZ")
                .withLocale(Locale.ROOT)
                .withChronology(ISOChronology.getInstanceUTC());
            dateTime = formatter.parseDateTime(isoDateTime);
        } catch(Exception e) {
            throw new PhoneBookException("AN EROR OCURRED WHILE CONVERTING DATE");
        }
        return dateTime;
    }

    public static String dateTimeStringtoIso (String stringDate) throws PhoneBookException {
        try {
            log.info("action=\"stringToIso\", description=\"method called\", stringToIso=\"{}\"", stringDate);
            if(stringDate == null) return null;
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
            DateTime dateTime = formatter.parseDateTime(stringDate.substring(0,10));
            String iso8601String = dateTime.toString();
            return iso8601String;
            /* TODO karim code for date - change it when we migrate to java 8
            SimpleDateFormat datetimeFormat =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            ISO8601DateFormat iSO8601DateFormat = new ISO8601DateFormat();
            return iSO8601DateFormat.format(datetimeFormat.parse(stringDate));*/
        } catch(Exception e) {
            throw new PhoneBookException("AN EROR OCURRED WHILE CONVERTING DATE");
        }

     }

    public static Timestamp dateTimeToTimestamp (DateTime dateTime) throws PhoneBookException {
        Timestamp timestamp;
        try{
            log.info("action=\"dateTimeToTimestamp\", description=\"method called\", dateTime=\"{}\"", dateTime);
            String stringDate = dateTime.toString().substring(0,10) + " 00:00:00";
            java.util.Date date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(stringDate);
            timestamp = new Timestamp(date.getTime());

        } catch(Exception e) {
            throw new PhoneBookException("AN EROR OCURRED WHILE CONVERTING DATE");
        }
        return timestamp;
    }
}
