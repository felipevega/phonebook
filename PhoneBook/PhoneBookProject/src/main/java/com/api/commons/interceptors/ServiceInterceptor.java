package com.api.commons.interceptors;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.spi.CorsHeaders;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;


@Provider
@NoArgsConstructor
@Slf4j
public class ServiceInterceptor implements ContainerRequestFilter, ContainerResponseFilter {



    public void filter(final ContainerRequestContext request,
                       final ContainerResponseContext response) throws IOException {
        if (!request.getMethod().equalsIgnoreCase("OPTIONS")) {

            response.getHeaders().add("Access-Control-Allow-Origin", "*");
            response.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
            response.getHeaders().add("Access-Control-Allow-Credentials", "true");
            response.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, HEAD, OPTIONS");
            log.info("action=\"filter\", description=\"Add CORS Filter Options\"");
        }
    }


    public void filter(ContainerRequestContext requestContext) throws IOException
    {
    }


}

