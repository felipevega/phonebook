package com.api.commons.util;

 import com.api.commons.repository.exceptions.CodificationException;
 import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

@Slf4j
public class Base64Encoder {

    private Base64Encoder () {
    }

    /**
     * Codifica el valor toString de un objeto en Base64.
     *
     * @param obj
     *            Objeto a codificar
     * @return Codificación del objeto
     *
     * @throws CodificationException Ocurre cuando hay un problema al codificar un elemento
     * @throws IllegalArgumentException Se produce cuando se intenta codificar un objeto null
     */
    public static <T> String encode(T obj) throws CodificationException, IllegalArgumentException {
        log.trace("Codificando en Base64 con urlSafe false");

        return encode(obj, false);
    }

    /**
     * Codifica el valor toString de un objeto en Base64.
     *
     * @param obj
     *            Objeto a codificar
     * @param urlSafe
     *            Si es true se usaran - y _ en vez de + y /.
     * @return Codificación del objeto
     *
     * @throws CodificationException Ocurre cuando hay un problema al codificar un elemento
     * @throws IllegalArgumentException Se produce cuando se intenta codificar un objeto null
     */
    public static <T> String encode(T obj, boolean urlSafe) throws CodificationException, IllegalArgumentException {
        if (obj == null) {
            throw new IllegalArgumentException("No se puede codificar un valor nulo");
        }

        log.trace("Codificando en Base64 con isChunked false y urlSafe [{}]", urlSafe);

        byte[] encoded = null;

        try {
            encoded = Base64.encodeBase64(obj.toString().getBytes(), false, urlSafe);
        } catch (IllegalArgumentException e) {
            log.error("No se pudo codificar el objeto", e);
            throw new CodificationException("Ocurrio un problema al codificar un objeto en base64");
        }

        return new String(encoded);
    }

    /**
     * Decodifica un String y retorna su valor decodificado.
     *
     * @param encodedString
     *            String a decodificar.
     *
     * @return String con el valor decodificado.
     */
    public static String decodeBase64String(String encodedString) {

        log.trace("Decodificando Base64 en String");

        String decodedString = null;

        if (encodedString != null) {
            byte[] decodedBytes = Base64.decodeBase64(encodedString.getBytes());
            decodedString = new String(decodedBytes);
        }

        return decodedString;
    }
}
