package com.api.commons.rest;

import javax.xml.bind.annotation.*;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceStatus {
    
    private int code = 0;
    
    @XmlElement(
            required = false,
            nillable = true
    )
    private String message = "";

    public ServiceStatus(int code) {
        this.code = code;
    }

    public ServiceStatus(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ServiceStatus() {
    }

    public int getCode() {
        return this.code;
    }
    
    public String getMessage() {
        return this.message;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean equals(Object o) {
        if(o == this) {
            return true;
        } else if(!(o instanceof ServiceStatus)) {
            return false;
        } else {
            ServiceStatus other = (ServiceStatus)o;
            if(!other.canEqual(this)) {
                return false;
            } else if(this.getCode() != other.getCode()) {
                return false;
            } else {
                String this$message = this.getMessage();
                String other$message = other.getMessage();
                if(this$message == null) {
                    if(other$message != null) {
                        return false;
                    }
                } else if(!this$message.equals(other$message)) {
                    return false;
                }

                return true;
            }
        }
    }

    public boolean canEqual(Object other) {
        return other instanceof ServiceStatus;
    }

    public int hashCode() {
        boolean PRIME = true;
        byte result = 1;
        int result1 = result * 31 + this.getCode();
        String $message = this.getMessage();
        result1 = result1 * 31 + ($message == null?0:$message.hashCode());
        return result1;
    }

    public String toString() {
        return "ServiceStatus(code=" + this.getCode() + ", message=" + this.getMessage() + ")";
    }
}
