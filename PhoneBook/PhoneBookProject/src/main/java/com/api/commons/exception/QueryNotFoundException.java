package com.api.commons.exception;

public class QueryNotFoundException extends DataAccessException {

    private static final long serialVersionUID = -9092786133511689341L;

    public QueryNotFoundException(String message) {
        super(message);
    }

    public QueryNotFoundException(Throwable cause) {
        super(cause);
    }

    public QueryNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
