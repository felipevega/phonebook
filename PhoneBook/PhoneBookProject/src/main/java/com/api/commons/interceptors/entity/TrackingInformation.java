package com.api.commons.interceptors.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;


@Component
@Scope(value = "request", proxyMode = TARGET_CLASS)
@Setter
@Getter
public class TrackingInformation {
    public static final String REQ_ID = "req-id";

    private String reqId;
}
