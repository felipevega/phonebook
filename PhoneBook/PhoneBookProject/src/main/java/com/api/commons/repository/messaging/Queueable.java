package com.api.commons.repository.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;

public abstract class Queueable {

    public abstract byte[] getMessage() throws JsonProcessingException;
}
