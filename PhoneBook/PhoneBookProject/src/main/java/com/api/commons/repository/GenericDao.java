package com.api.commons.repository;


import com.api.commons.exception.PhoneBookException;

import java.io.Serializable;

public interface GenericDao<T> extends RoDao<T> {

    void save(T obj) throws PhoneBookException;
    void update(T obj) throws PhoneBookException;
    void delete(T obj) throws PhoneBookException;

    /**
     * Deletes entity by id
     * @param id
     * @throws PhoneBookException
     */
    void delete(Serializable id) throws PhoneBookException;
}
