package com.api.commons.interceptors;

import javax.ws.rs.core.Feature;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;

import lombok.extern.slf4j.Slf4j;
import org.jboss.resteasy.plugins.interceptors.CorsFilter;

@Slf4j
@Provider
public class CorsFeature implements Feature {


    public boolean configure(FeatureContext context) {
        CorsFilter corsFilter = new CorsFilter();
        //corsFilter.getAllowedOrigins().add("http://52.170.103.122");
        corsFilter.getAllowedOrigins().add("*");
        context.register(corsFilter);
        log.info("action=\"CorsFeature\", description=\"configure allow origin\"");
        return true;
    }
}