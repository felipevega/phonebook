package com.api.commons.rest;


import com.api.phonebook.domain.Contact;

import java.util.*;
import javax.xml.bind.annotation.*;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class JSONResponse<T> {
    private ServiceStatus status;
    private T data;

    public JSONResponse() {
    }

    public ServiceStatus getStatus() {
        return this.status;
    }

    public T getData() {
        return this.data;
    }

    public void setStatus(ServiceStatus status) {
        this.status = status;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean equals(Object o) {
        if(o == this) {
            return true;
        } else if(!(o instanceof JSONResponse)) {
            return false;
        } else {
            JSONResponse other = (JSONResponse)o;
            if(!other.canEqual(this)) {
                return false;
            } else {
                ServiceStatus this$status = this.getStatus();
                ServiceStatus other$status = other.getStatus();
                if(this$status == null) {
                    if(other$status != null) {
                        return false;
                    }
                } else if(!this$status.equals(other$status)) {
                    return false;
                }

                Object this$data = this.getData();
                Object other$data = other.getData();
                if(this$data == null) {
                    if(other$data == null) {
                        return true;
                    }
                } else if(this$data.equals(other$data)) {
                    return true;
                }

                return false;
            }
        }
    }

    public boolean canEqual(Object other) {
        return other instanceof JSONResponse;
    }

    public int hashCode() {
        boolean PRIME = true;
        byte result = 1;
        ServiceStatus $status = this.getStatus();
        int result1 = result * 31 + ($status == null?0:$status.hashCode());
        Object $data = this.getData();
        result1 = result1 * 31 + ($data == null?0:$data.hashCode());
        return result1;
    }

    public String toString() {
        return "JSONResponse(status=" + this.getStatus() + ", data=" + this.getData() + ")";
    }
}
