package com.api.commons.repository.exceptions;

public class CodificationException extends Exception {

    private static final long serialVersionUID = 7599944499371536561L;

    public CodificationException(String msg) {
        super(msg);
    }
}
