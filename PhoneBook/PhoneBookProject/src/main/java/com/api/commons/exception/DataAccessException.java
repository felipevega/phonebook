package com.api.commons.exception;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class DataAccessException extends PhoneBookException {

    /**
     * Serial Este valor es autogenerado por java para permitir serializar este
     * objeto.
     */
    private static final long serialVersionUID = 2103752610293954716L;

    /**
     * Constructor Crea una DataAccessException con un mensaje de error
     * definido.
     */
    public DataAccessException() {
        super("Error en el acceso a los datos.");
    }

    /**
     * Constructor Crea una DataAccessException con un mensaje de error
     * definido.
     *
     * @param message
     *            El mensaje de error.
     */
    public DataAccessException(String message) {
        super(message);
    }

    /**
     * Calls PhoneBookException(Throwable e)
     *
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            Throwable.getCause() method). (A null value is permitted, and
     *            indicates that the cause is nonexistent or unknown.)
     */
    public DataAccessException(Throwable cause) {
        super(cause);
    }

    public DataAccessException(String message, Throwable cause) {
        super(message, cause);
    }

}
