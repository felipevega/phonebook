package com.api.commons.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.Serializable;

public abstract class AbstractGenericDao<T> extends AbstractRoDao<T> implements GenericDao<T> {

    protected AbstractGenericDao(Class<T> entityClass, SessionFactory sessionFactory) {
        super(entityClass, sessionFactory);
    }
    public void save(T obj) {
        getCurrentSession().save(obj);
    }
    public void update(T obj) {
        getCurrentSession().update(obj);
    }

    public void delete(Serializable id) {
        Session session = getCurrentSession();
        session.delete(session.get(getClazz(), id));
    }

    public void delete(T obj) {
        getCurrentSession().delete(obj);
    }
}