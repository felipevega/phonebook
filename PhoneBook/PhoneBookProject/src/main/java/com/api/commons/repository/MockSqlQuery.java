package com.api.commons.repository;

import org.hibernate.*;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * Clase mock de SQLQuery 
 *
 *
 * 		public void testDaoOk() {
 * 			RouteDaoRO routeDaoRO = new RouteDaoRO(null, new HashMap<String, String>()) {
 * 				public SQLQuery createSqlQuery(String methodName) {
 * 					final List<Route> results = new ArrayList<>();
 * 					results.add(new Route());
 * 					results.add(new Route());
 *
 * 					MockSqlQuery<Route> query = new MockSqlQuery<Route>();
 * 					query.setResult(results);
 *
 * 					return query;
 * 				}
 * 			};
 *
 * 			List<Route> routes = routeDaoRO.searchGroupedRoute("query");
 * 			assertEquals(2, routes.size());
 * 		}
 *
 * @author msalazar
 *
 * @param <E> Tipo de dato que se usara en el mapeo de las consultas
 */
@SuppressWarnings("rawtypes")
public abstract class MockSqlQuery<E> implements SQLQuery {

    private List<E> results;

    private List<E> getResults() {
        if (results == null) {
            results = new ArrayList();
        }
        return results;
    }

    /**
     * Agrega los resultados que se quieren simular de la ejecución de una query
     *
     * @param results Conjunto de entidades que usaran como mock de una query
     */
    public void setResult(List<E> results) {
        this.results = results;
    }

     
    public String getQueryString() {
        return null;
    }

     
    public Type[] getReturnTypes() throws HibernateException {
        return null;
    }

     
    public String[] getReturnAliases() throws HibernateException {
        return null;
    }

     
    public String[] getNamedParameters() throws HibernateException {
        return null;
    }

     
    public Iterator iterate() throws HibernateException {
        return null;
    }

     
    public ScrollableResults scroll() throws HibernateException {
        return null;
    }

     
    public ScrollableResults scroll(ScrollMode scrollMode)
            throws HibernateException {
        return null;
    }

     
    public List list() throws HibernateException {
        return getResults();
    }

     
    public Object uniqueResult() throws HibernateException {
        int size = getResults().size();

        if (size==0) {
            return null;
        }

        if (size != 1) {
            throw new NonUniqueResultException(size);
        }

        return getResults().get(0);
    }

     
    public int executeUpdate() throws HibernateException {
        return 0;
    }

     
    public Query setMaxResults(int maxResults) {
        return null;
    }

     
    public Query setFirstResult(int firstResult) {
        return null;
    }

     
    public boolean isReadOnly() {
        return false;
    }

     
    public Query setReadOnly(boolean readOnly) {
        return null;
    }

     
    public Query setCacheable(boolean cacheable) {
        return null;
    }

     
    public Query setCacheRegion(String cacheRegion) {
        return null;
    }

     
    public Query setTimeout(int timeout) {
        return null;
    }

     
    public Query setFetchSize(int fetchSize) {
        return null;
    }

     
    public LockOptions getLockOptions() {
        return null;
    }

     
    public Query setLockOptions(LockOptions lockOptions) {
        return null;
    }

     
    public Query setLockMode(String alias, LockMode lockMode) {
        return null;
    }

     
    public Query setComment(String comment) {
        return null;
    }

     
    public Query setFlushMode(FlushMode flushMode) {
        return null;
    }

     
    public Query setCacheMode(CacheMode cacheMode) {
        return null;
    }

     
    public Query setParameter(int position, Object val, Type type) {
        return null;
    }

     
    public Query setParameter(String name, Object val, Type type) {
        return null;
    }

     
    public Query setParameter(int position, Object val)
            throws HibernateException {
        return null;
    }

     
    public Query setParameter(String name, Object val)
            throws HibernateException {
        return null;
    }

     
    public Query setParameters(Object[] values, Type[] types)
            throws HibernateException {
        return null;
    }

     
    public Query setParameterList(String name, Collection vals, Type type)
            throws HibernateException {
        return null;
    }

     
    public Query setParameterList(String name, Collection vals)
            throws HibernateException {
        return null;
    }

     
    public Query setParameterList(String name, Object[] vals, Type type)
            throws HibernateException {
        return null;
    }

     
    public Query setParameterList(String name, Object[] vals)
            throws HibernateException {
        return null;
    }

     
    public Query setProperties(Object bean) throws HibernateException {
        return null;
    }

     
    public Query setProperties(Map bean) throws HibernateException {
        return null;
    }

     
    public Query setString(int position, String val) {
        return null;
    }

     
    public Query setCharacter(int position, char val) {
        return null;
    }

     
    public Query setBoolean(int position, boolean val) {
        return null;
    }

     
    public Query setByte(int position, byte val) {
        return null;
    }

     
    public Query setShort(int position, short val) {
        return null;
    }

     
    public Query setInteger(int position, int val) {
        return null;
    }

     
    public Query setLong(int position, long val) {
        return null;
    }

     
    public Query setFloat(int position, float val) {
        return null;
    }

     
    public Query setDouble(int position, double val) {
        return null;
    }

     
    public Query setBinary(int position, byte[] val) {
        return null;
    }

     
    public Query setText(int position, String val) {
        return null;
    }

     
    public Query setSerializable(int position, Serializable val) {
        return null;
    }

     
    public Query setLocale(int position, Locale locale) {
        return null;
    }

     
    public Query setBigDecimal(int position, BigDecimal number) {
        return null;
    }

     
    public Query setBigInteger(int position, BigInteger number) {
        return null;
    }

     
    public Query setDate(int position, Date date) {
        return null;
    }

     
    public Query setTime(int position, Date date) {
        return null;
    }

     
    public Query setTimestamp(int position, Date date) {
        return null;
    }

     
    public Query setCalendar(int position, Calendar calendar) {
        return null;
    }

     
    public Query setCalendarDate(int position, Calendar calendar) {
        return null;
    }

     
    public Query setString(String name, String val) {
        return null;
    }

     
    public Query setCharacter(String name, char val) {
        return null;
    }

     
    public Query setBoolean(String name, boolean val) {
        return null;
    }

     
    public Query setByte(String name, byte val) {
        return null;
    }

     
    public Query setShort(String name, short val) {
        return null;
    }

     
    public Query setInteger(String name, int val) {
        return null;
    }

     
    public Query setLong(String name, long val) {
        return null;
    }

     
    public Query setFloat(String name, float val) {
        return null;
    }

     
    public Query setDouble(String name, double val) {
        return null;
    }

     
    public Query setBinary(String name, byte[] val) {
        return null;
    }

     
    public Query setText(String name, String val) {
        return null;
    }

     
    public Query setSerializable(String name, Serializable val) {
        return null;
    }

     
    public Query setLocale(String name, Locale locale) {
        return null;
    }

     
    public Query setBigDecimal(String name, BigDecimal number) {
        return null;
    }

     
    public Query setBigInteger(String name, BigInteger number) {
        return null;
    }

     
    public Query setDate(String name, Date date) {
        return null;
    }

     
    public Query setTime(String name, Date date) {
        return null;
    }

     
    public Query setTimestamp(String name, Date date) {
        return null;
    }

     
    public Query setCalendar(String name, Calendar calendar) {
        return null;
    }

     
    public Query setCalendarDate(String name, Calendar calendar) {
        return null;
    }

     
    public Query setEntity(int position, Object val) {
        return null;
    }

     
    public Query setEntity(String name, Object val) {
        return null;
    }

     
    public Query setResultTransformer(ResultTransformer transformer) {
        return null;
    }

     
    public SQLQuery addSynchronizedQuerySpace(String querySpace) {
        return null;
    }

     
    public SQLQuery addSynchronizedEntityName(String entityName)
            throws MappingException {
        return null;
    }

     
    public SQLQuery addSynchronizedEntityClass(Class entityClass)
            throws MappingException {
        return null;
    }

     
    public SQLQuery setResultSetMapping(String name) {
        return null;
    }

     
    public SQLQuery addScalar(String columnAlias) {
        return null;
    }

     
    public SQLQuery addScalar(String columnAlias, Type type) {
        return null;
    }

     
    public RootReturn addRoot(String tableAlias, String entityName) {
        return null;
    }

     
    public RootReturn addRoot(String tableAlias, Class entityType) {
        return null;
    }

     
    public SQLQuery addEntity(String entityName) {
        return null;
    }

     
    public SQLQuery addEntity(String tableAlias, String entityName) {
        return null;
    }

     
    public SQLQuery addEntity(String tableAlias, String entityName,
                              LockMode lockMode) {
        return null;
    }

     
    public SQLQuery addEntity(Class entityType) {
        return null;
    }

     
    public SQLQuery addEntity(String tableAlias, Class entityType) {
        return null;
    }

     
    public SQLQuery addEntity(String tableAlias, Class entityName,
                              LockMode lockMode) {
        return null;
    }

     
    public FetchReturn addFetch(String tableAlias, String ownerTableAlias,
                                String joinPropertyName) {
        return null;
    }

     
    public SQLQuery addJoin(String tableAlias, String path) {
        return null;
    }

     
    public SQLQuery addJoin(String tableAlias, String ownerTableAlias,
                            String joinPropertyName) {
        return null;
    }

    
    public SQLQuery addJoin(String tableAlias, String path, LockMode lockMode) {
        return null;
    }

}
