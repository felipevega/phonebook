package com.api.commons.repository.exceptions;

public class InvalidPersistenceParamException extends Exception {

    private static final long serialVersionUID = -2650204232467971404L;

    @Override
    public String getMessage() {
        return "Problem occurred using an parameter in persistence query";
    }
}
