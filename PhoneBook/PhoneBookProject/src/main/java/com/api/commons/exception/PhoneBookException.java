package com.api.commons.exception;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class PhoneBookException extends Exception {

    /**
     * Serial Este valor es autogenerado por java para permitir serializar este
     * objeto.
     */
    private static final long serialVersionUID = 2103752610293954716L;

    /**
     * Descripción Descripción detallada del mensaje de error. Debe ser
     * entendida por una persona.
     */
    private String description;

    /**
     * Estado Código identificador del estado de la excepción.
     */
    private int status;

    /**
     * Constructor Crea una PhoneBookException con un mensaje de error dererminado.
     *
     * @param message
     *            El mensaje de error.
     */
    public PhoneBookException(String message) {
        super(message);
    }

    /**
     * Constructor Crea una PhoneBookException con un mensaje de error dererminado.
     *
     * @param message
     *            El mensaje de error.
     * @param e
     *            the cause (which is saved for later retrieval by the
     *            Throwable.getCause() method). (A null value is permitted, and
     *            indicates that the cause is nonexistent or unknown.)
     */
    public PhoneBookException(String message, Throwable e) {
        super(message, e);
    }

    /**
     * Calls Exception(Throwable e)
     *
     * @param cause
     *            the cause (which is saved for later retrieval by the
     *            Throwable.getCause() method). (A null value is permitted, and
     *            indicates that the cause is nonexistent or unknown.)
     */
    public PhoneBookException(Throwable cause) {
        super(cause);
    }
}

