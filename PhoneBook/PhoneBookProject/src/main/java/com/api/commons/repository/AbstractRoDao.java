package com.api.commons.repository;

import com.api.commons.repository.exceptions.NotFoundException;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class AbstractRoDao<T> extends AbstractDao<T> implements RoDao<T> {
    private Class<T> clazz;

    protected AbstractRoDao(Class<T> entityClass, SessionFactory sessionFactory) {
        super(entityClass, sessionFactory, Collections.<String, String> emptyMap());
        clazz = entityClass;
    }

     
    public List<T> all() {
        Criteria criteria = createCriteria();
        criteria.addOrder(Order.asc("id"));

        return criteria.list();
    }

     
    public T findUniqueBy(String name, Object value) throws NotFoundException {
        List<T> entities = findBy(name, value);
        if (entities.isEmpty()) {
            throw createNotFoundException(name, value);
        } else {
            return entities.get(0);
        }
    }

     
    public List<T> findBy(String name, Object value) {
        return createByCriteria(name, value).list();
    }

    private Criteria createByCriteria(String name, Object value) {
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq(name, value));
        return criteria;
    }

    private Criteria createCriteria() {
        return getCurrentSession().createCriteria(getClazz());
    }

     
    public T findById(Serializable id) throws NotFoundException {
        T result = (T) getCurrentSession().byId(getClazz()).load(id);
        if (result == null) {
            throw createNotFoundException("id", id);
        }
        return result;
    }

    private NotFoundException createNotFoundException(String parameter, Object value) {
        return new NotFoundException("Not entity found of type " + getClazz().getSimpleName() + " with parameter: name = " + parameter + " and value = " + value);
    }

    protected Class<T> getClazz() {
        return clazz;
    }
}

