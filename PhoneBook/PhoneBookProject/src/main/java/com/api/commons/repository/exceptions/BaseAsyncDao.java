package com.api.commons.repository.exceptions;


import com.api.commons.exception.QueryNotFoundException;
import com.api.commons.repository.AsyncQuery;
import com.api.commons.repository.messaging.AmqpProducer;

import java.util.HashMap;
import java.util.Map;


public abstract class BaseAsyncDao {

    private Map<String, String> queries;
    protected AmqpProducer queue;

    public BaseAsyncDao(AmqpProducer r, Map<String, String> q) {
        this.queue = r;
        setQueries(q);
    }

    protected Map<String, String> getQueries() {
        if (queries == null) {
            queries = new HashMap<String, String>();
        }

        return queries;
    }

    protected AsyncQuery createSQLQuery(String methodName)
            throws QueryNotFoundException {
        String sql = getQueries().get(methodName);

        if (sql == null)
            throw new QueryNotFoundException("La query [" + methodName
                    + "] no ha sido encontrada");
        return new AsyncQuery(sql);
    }

    private void setQueries(Map<String, String> q) {
        this.queries = q != null ? q : new HashMap<String, String>();

    }
}
