package com.api.commons.repository;

import java.util.ArrayList;
import java.util.List;

import com.api.commons.repository.exceptions.CodificationException;
import com.api.commons.repository.exceptions.InvalidPersistenceParamException;
import com.api.commons.repository.messaging.Queueable;
import com.api.commons.util.Base64Encoder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@Slf4j
public class AsyncQuery extends Queueable {

    @Getter(lazy = true)
    private final List<String> parameters = new ArrayList<String>();
    @Getter
    private String query;

    public AsyncQuery(String q) {
        query = q;
    }

    public <T> void addParameter(int pos, T value) throws InvalidPersistenceParamException {
        try {
            getParameters().add(pos, Base64Encoder.encode(value));
        } catch (IllegalArgumentException e) {
            getParameters().add(pos, null);
            log.info("getParameters().add({}, Base64Encoder.encode({})) threw: {}", pos, value, e.getMessage());
        } catch (CodificationException e) {
            log.error("No se pudo asignar el parametro [" + value + "]", e);
            throw new InvalidPersistenceParamException();
        }
    }

    @Override
    @JsonIgnore
    // JsonIgnore is needed because jackson uses get methods to create the json
    // representation, not having it produces an infinite loop calling this
    // function
    public byte[] getMessage() throws JsonProcessingException {
        int params = StringUtils.countOccurrencesOf(this.getQuery(), "?");
        if (params == getParameters().size()) {
            ObjectWriter ow = new ObjectMapper().writer();
            String json = ow.writeValueAsString(this);
            return json.getBytes();
        }
        throw new IllegalArgumentException(
                "La cantidad de parametros definidos en la consulta, no es la misma que los entregamos como parametros");

    }

}
