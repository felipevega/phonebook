package com.api.commons.repository.exceptions;

import com.api.commons.exception.PhoneBookException;

public class NotFoundException extends PhoneBookException {
    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(String message, Throwable e) {
        super(message, e);
    }

    public NotFoundException(Throwable cause) {
        super(cause);
    }
}