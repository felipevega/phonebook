package com.api.phonebook.interceptor;

import com.api.commons.rest.JSONResponse;
import com.api.commons.rest.ServiceStatus;
import com.api.phonebook.exceptions.PhoneBookException;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Component
public class PhoneBookExceptionMapper implements ExceptionMapper<PhoneBookException> {

    public Response toResponse(PhoneBookException e) {
        JSONResponse response = new JSONResponse();
        response.setStatus(new ServiceStatus(e.getCode(), e.getMessage()));
        return Response.status(200).entity(response).build();
    }
}

