package com.api.phonebook.service;


import com.api.phonebook.domain.Contact;

import java.util.List;

public interface IContactService {

    List<Contact> getContacts(String firstName);

    Integer addContact(Contact contact);
}
