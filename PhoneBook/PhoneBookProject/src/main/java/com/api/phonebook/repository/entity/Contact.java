package com.api.phonebook.repository.entity;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@ToString
public class Contact {
    @Id
    private Integer id;
    private String firstName;
    private String lastName;
    private String phone;
}
