package com.api.phonebook.repository;

import com.api.commons.repository.AbstractDao;
import com.api.phonebook.exceptions.PhoneBookException;
import com.api.phonebook.repository.entity.Contact;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Slf4j
@Repository
@NoArgsConstructor
@Transactional
public class ContactDao extends AbstractDao<Contact> {

    @Autowired
    public ContactDao(
            @Qualifier("sessionFactoryCentralRo") SessionFactory sessionFactoryCentralRo,
            @Value("#{objectQueries}") Map<String, String> queries) {
        super(Contact.class, sessionFactoryCentralRo, queries);
    }

    public List<Contact> getContactsList(String firstName) {
        List<Contact> contacts;
        try {
            SQLQuery query = createSqlQueryWithEntity("getContactsList");
            contacts = query.list();
            if (!firstName.isEmpty()) {
                List<Contact> filtered = new ArrayList<Contact>();
                for(Contact contact : contacts) {
                    if(contact.getFirstName().toLowerCase().contains(firstName.toLowerCase())) {
                        filtered.add(contact);
                    }
                }
                return filtered;
            }
            log.info("action=\"getContactsList\", description=\"dao called\", objects=\"{}\"", contacts.toString());
        } catch (Exception e) {
            log.error("action=\"getObject\", description=\"Error accessing the database\"", e);
            throw new PhoneBookException(500, "AN EROR OCURRED WHILE ACCESSING THE DATABASE");
        }
        return contacts;
     }

    public Integer addContact(Contact contact) {
        Integer insertedContactId=0;
        try {
            SQLQuery query = createSqlQuery("addContact");
            query.setParameter("firstName", contact.getFirstName());
            query.setParameter("lastName", contact.getLastName());
            query.setParameter("phone", contact.getPhone());
            query.uniqueResult();
            log.info("action=\"addContact\", description=\"dao called\", addContact=\"{}\"", contact.toString());

        } catch (Exception e) {
            log.error("action=\"addContact\", description=\"Error accessing the database\"", e);
            throw new PhoneBookException(500, "AN EROR OCURRED WHILE ACCESSING THE DATABASE");
        }
        return insertedContactId;
    }
}
