package com.api.phonebook.exceptions;


public class PhoneBookException extends RuntimeException {

    private final int code;

    public PhoneBookException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }

}
