package com.api.phonebook.service;


import com.api.phonebook.domain.Contact;
import com.api.phonebook.repository.ContactDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ContactService implements IContactService {

    @Autowired
    ContactDao contactDao;

    public List<Contact> getContacts(String firstName){
        return map(contactDao.getContactsList(firstName));

    }

    public Integer addContact(Contact contact) {
        com.api.phonebook.repository.entity.Contact contactDb = new com.api.phonebook.repository.entity.Contact();
        contactDb.setFirstName(contact.getFirstName());
        contactDb.setLastName(contact.getLastName());
        contactDb.setPhone(contact.getPhone());
        return contactDao.addContact(contactDb);
    }

    private List<Contact> map(List<com.api.phonebook.repository.entity.Contact> entityContacts) {
        List<Contact> contacts = new ArrayList();
        for (com.api.phonebook.repository.entity.Contact entityContact : entityContacts) {
            Contact contact = new Contact();
            contact.setFirstName(entityContact.getFirstName());
            contact.setLastName(entityContact.getLastName());
            contact.setId(entityContact.getId());
            contact.setPhone(entityContact.getPhone());
            contacts.add(contact);
        }
        return contacts;
    }

}
