package com.api.phonebook.controller;


import com.api.commons.rest.JSONResponse;
import com.api.commons.rest.ServiceStatus;
import com.api.phonebook.domain.Contact;
import com.api.phonebook.service.IContactService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Controller
@Path("/")
@Slf4j
public class ContactController {


    @Autowired
    IContactService contactService;


    @GET
    @Path("/contacts")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContacts(@QueryParam("firstName") @DefaultValue("")  String firstName) {
        log.info("action=\"getContact\", description=\"getContact controller\", endpoint=\"/contacts\"");
        List<Contact> contacts = contactService.getContacts(firstName);
        JSONResponse<List<Contact>> response = new JSONResponse();
        response.setStatus(new ServiceStatus(200));
        response.setData(contacts);
        Response.ResponseBuilder responseBuilder = Response.status(200).entity(response);
        return responseBuilder.build();
    }

    @POST
    @Path("/contact/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addContact(Contact contact) {
        log.info("action=\"addContact\", description=\"addContact controller\", endpoint=\"/addContact\"");
        Integer insertedContactId = contactService.addContact(contact);
        JSONResponse<Integer> response = new JSONResponse();
        response.setStatus(new ServiceStatus(200));
        response.setData(insertedContactId);
        Response.ResponseBuilder responseBuilder = Response.status(200).entity(response);
        log.info("action=\"addContact\", description=\"controller addContact\", contactInsertedId=\"{}\"", insertedContactId.toString());
        return responseBuilder.build();
    }


}
