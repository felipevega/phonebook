CREATE SEQUENCE contact_seq;
CREATE TABLE contact(
  id int default nextval('contact_seq'::regclass),
  firstName varchar,
  lastName varchar,
  phone varchar
);

INSERT INTO contact (firstName, lastName, phone) VALUES ('Arun', 'Kart', '415-8679089' );
INSERT INTO contact (firstName, lastName, phone) VALUES ('Juan', 'Torus', '301-2390930' );
INSERT INTO contact (firstName, lastName, phone) VALUES ('Nolux', 'Fernandez', '310-2930291' );
INSERT INTO contact (firstName, lastName, phone) VALUES ('Daniela', 'Marin', '310-2930291' );


CREATE OR REPLACE FUNCTION public.addContact(
    firstName character,
    lastName character,
    phone character)
  RETURNS integer AS
$BODY$
declare 
	nextContactId integer;
	insertedContactId integer;

BEGIN

   BEGIN
   
   	SELECT nextval('contact_seq') INTO nextContactId;

	INSERT INTO contact 
		(id,
		firstName,
		lastName,
		phone) 
		VALUES 
		(nextContactId,
		firstName,
		lastName,
		phone) 
		RETURNING id INTO insertedContactId;
 
   END;	
   RETURN insertedContactId;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.addContact(character, character, character)
  OWNER TO postgres;

